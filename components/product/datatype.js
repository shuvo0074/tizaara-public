let categories={
    categories:[
        {
        "id": 25,
        "parent_id": 24,
        "name": "Industrial Machinery & Tools",
        "image": "/upload/category/5efcb8379678b.png",
        "description": "Industrial Machinery & Tools",
        "rank": 1,
        "status": 1,
        "created_by": 1,
        "updated_by": 1,
        "deleted_at": null,
        "created_at": "2020-07-01T16:22:15.000000Z",
        "updated_at": "2020-07-01T16:23:07.000000Z",
        "ip_address": "103.84.38.250",
        "children": []
    },
    {
        "id": 6,
        "parent_id": 2,
        "name": "Aluminum Profiles",
        "image": null,
        "description": null,
        "rank": 2,
        "status": 1,
        "created_by": null,
        "updated_by": null,
        "deleted_at": null,
        "created_at": "2020-06-22T06:09:19.000000Z",
        "updated_at": null,
        "ip_address": null,
        "children": []
    },
    {
        "id": 7,
        "parent_id": 2,
        "name": "Mild Steel Angles ",
        "image": null,
        "description": null,
        "rank": 3,
        "status": 1,
        "created_by": null,
        "updated_by": null,
        "deleted_at": null,
        "created_at": "2020-06-22T06:10:00.000000Z",
        "updated_at": null,
        "ip_address": null,
        "children": []
    }
] // these are selected categories
}

let simpleProduct= {
    "name":"This is a simple product",
    "keywords":["array","array1","array2","array3"],
    "productTypes":[
        {
            "terms":["Simple"],
            "image": [
                "/upload/category/5efcbda47a059.png",
                "/upload/category/5efcbda47a059.png",
                "/upload/category/5efcbda47a059.png",
                "/upload/category/5efcbda47a059.png",
                "/upload/category/5efcbda47a059.png",
                "/upload/category/5efcbda47a059.png"
            ],
            "productInStock":100,
            "productCondition":"used",
            "minimumOrder":100,
            "price":[
                {
                "rangeMinimum":1,
                "rangeMaximum":100, 
                "currency":"BDT",
                "amount":2000
                }
            ]            
        }
    ]

    }

    let attributedProducts= {
        "name":"These are products with different attributes and terms",
        "keywords":["key1","key2","key3","key4"],
        "productTypes":[
            {
                "terms":["red","xxl"],
                "image": [
                    "/upload/category/5efcbda47a059.png"
                ],
                "productInStock":100,
                "productCondition":"used",
                "minimumOrder":100,
                "price":[
                    {
                    "rangeMinimum":1,
                    "rangeMaximum":100, 
                    "currency":"BDT",
                    "amount":2000
                    }
                ]            
            },
            {
                "terms":["red","xl"],
                "image": [
                    "/upload/category/5efcbda47a059.png"
                ],
                "productInStock":100,
                "productCondition":"used",
                "minimumOrder":100,
                "price":[
                    {
                    "rangeMinimum":1,
                    "rangeMaximum":100, 
                    "currency":"BDT  (These are products with volume tier pricing)",
                    "amount":2000
                    },
                    {
                        "rangeMinimum":101,
                        "rangeMaximum":200, 
                        "currency":"BDT  (These are products with volume tier pricing)",
                        "amount":2200
                    }
                ]            
            },
            {
                "terms":["blue","xl"],
                "image": [
                    "/upload/category/5efcbda47a059.png"
                ],
                "productInStock":1000,
                "productCondition":"used",
                "minimumOrder":1,
                "price":[
                    {
                    "rangeMinimum":1,
                    "rangeMaximum":100, 
                    "currency":"BDT  (These are products with volume tier pricing)",
                    "amount":2000
                    },
                    {
                        "rangeMinimum":101,
                        "rangeMaximum":500, 
                        "currency":"BDT  (These are products with volume tier pricing)",
                        "amount":1900
                    },
                    {
                        "rangeMinimum":501,
                        "rangeMaximum":1000, 
                        "currency":"BDT  (These are products with volume tier pricing)",
                        "amount":1800
                    }
                ]            
            }
        ]
    
    }
    let productDetails={
        brands: [
            {
              name: 'b2',
              selected: false
            },
            {
              name: 'b3',
              selected: false
            },
            {
              name: 'b4',
              selected: true
            },
            {
              name: 'b0',
              selected: false
            },
            {
              name: 'Other',
              selected: false
            }
        ],
        productDescription:'productDescription',
        productConnectivity:"productConnectivity",
        productColor:"",
        productModel:'',
        productDisplay:'',
        productMaterial:'',
        productPowerConsumtion:'',
        productInfo:[{
            title:"inf title 1",
            value:"info value 1"
          },{
            title:"inf title 2",
            value:"info value 2"
          },{
            title:"inf title 3",
            value:"info value 3"
          },{
            title:"inf title 4",
            value:"info value 4"
          },
          ]
    }
    let additionalDetails={
        paymentTerms:[
            {
              name: 'pt1',
              selected: false
            },
            {
              name: 'pt2',
              selected: false
            },
            {
              name: 'pt3',
              selected: true
            },
            {
              name: 'pt4',
              selected: false
            },
            {
              name: 'Other',
              selected: false
            }
        ],
        productCode:'',
        processingTime:25,
        portDispatch:'',
        supplyAbility:5000,
        pieces:'4',
        week:'3',
        packagingDetails:"This is the packagng details"
    }
let fullProduct={
    categories:[
        {
        "id": 25,
        "parent_id": 24,
        "name": "Industrial Machinery & Tools",
        "image": "/upload/category/5efcb8379678b.png",
        "description": "Industrial Machinery & Tools",
        "rank": 1,
        "status": 1,
        "created_by": 1,
        "updated_by": 1,
        "deleted_at": null,
        "created_at": "2020-07-01T16:22:15.000000Z",
        "updated_at": "2020-07-01T16:23:07.000000Z",
        "ip_address": "103.84.38.250",
        "children": []
    },
    {
        "id": 6,
        "parent_id": 2,
        "name": "Aluminum Profiles",
        "image": null,
        "description": null,
        "rank": 2,
        "status": 1,
        "created_by": null,
        "updated_by": null,
        "deleted_at": null,
        "created_at": "2020-06-22T06:09:19.000000Z",
        "updated_at": null,
        "ip_address": null,
        "children": []
    },
    {
        "id": 7,
        "parent_id": 2,
        "name": "Mild Steel Angles ",
        "image": null,
        "description": null,
        "rank": 3,
        "status": 1,
        "created_by": null,
        "updated_by": null,
        "deleted_at": null,
        "created_at": "2020-06-22T06:10:00.000000Z",
        "updated_at": null,
        "ip_address": null,
        "children": []
    }
    ],
    name:"These are products with different attributes and terms",
    keywords:["key1","key2","key3","key4"],
    productTypes:[
        {
            "terms":["red","xxl"],
            "image": [
                "/upload/category/5efcbda47a059.png"
            ],
            "productInStock":100,
            "productCondition":"used",
            "minimumOrder":100,
            "price":[
                {
                "rangeMinimum":1,
                "rangeMaximum":100, 
                "currency":"BDT",
                "amount":2000
                }
            ]            
        },
        {
            "terms":["red","xl"],
            "image": [
                "/upload/category/5efcbda47a059.png"
            ],
            "productInStock":100,
            "productCondition":"used",
            "minimumOrder":100,
            "price":[
                {
                "rangeMinimum":1,
                "rangeMaximum":100, 
                "currency":"BDT  (These are products with volume tier pricing)",
                "amount":2000
                },
                {
                    "rangeMinimum":101,
                    "rangeMaximum":200, 
                    "currency":"BDT  (These are products with volume tier pricing)",
                    "amount":2200
                }
            ]            
        },
        {
            "terms":["blue","xl"],
            "image": [
                "/upload/category/5efcbda47a059.png"
            ],
            "productInStock":1000,
            "productCondition":"used",
            "minimumOrder":1,
            "price":[
                {
                "rangeMinimum":1,
                "rangeMaximum":100, 
                "currency":"BDT  (These are products with volume tier pricing)",
                "amount":2000
                },
                {
                    "rangeMinimum":101,
                    "rangeMaximum":500, 
                    "currency":"BDT  (These are products with volume tier pricing)",
                    "amount":1900
                },
                {
                    "rangeMinimum":501,
                    "rangeMaximum":1000, 
                    "currency":"BDT  (These are products with volume tier pricing)",
                    "amount":1800
                }
            ]            
        }
    ],
    brands: [
        {
          name: 'b2',
          selected: false
        },
        {
          name: 'b3',
          selected: false
        },
        {
          name: 'b4',
          selected: true
        },
        {
          name: 'b0',
          selected: false
        },
        {
          name: 'Other',
          selected: false
        }
    ],
    productDescription:'productDescription',
    productConnectivity:"productConnectivity",
    productColor:"",
    productModel:'',
    productDisplay:'',
    productMaterial:'',
    productPowerConsumtion:'',
    productInfo:[{
        title:"inf title 1",
        value:"info value 1"
      },{
        title:"inf title 2",
        value:"info value 2"
      },{
        title:"inf title 3",
        value:"info value 3"
      },{
        title:"inf title 4",
        value:"info value 4"
      },
    ],
    paymentTerms:[
        {
          name: 'pt1',
          selected: false
        },
        {
          name: 'pt2',
          selected: false
        },
        {
          name: 'pt3',
          selected: true
        },
        {
          name: 'pt4',
          selected: false
        },
        {
          name: 'Other',
          selected: false
        }
    ],
    productCode:'',
    processingTime:25,
    portDispatch:'',
    supplyAbility:5000,
    pieces:'4',
    week:'3',
    packagingDetails:"This is the packagng details"
}