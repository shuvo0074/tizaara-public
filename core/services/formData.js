/**
 * @param {Object} items The date
 * @param data
 * @param name
 */
const formData = (items, data = new FormData(), name = '') => {
  items = Array.isArray(items) ? {...items} : items;
  items['_photoIndex'] = items['_photoIndex'] ? items['_photoIndex'] : 'photo';
  for (let key in items) {
    if (items[key] && typeof items[key] === 'object' && (key !== items['_photoIndex'])) {
      formData(items[key], data, key)
    } else if (typeof items[key] != 'undefined' && items[key] !== null)
      data.append(name ? name + '[' + key + ']' : key, items[key]);
  }
  return data;
}
export default formData;
