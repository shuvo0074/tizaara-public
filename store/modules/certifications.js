import vue from 'vue'
import {SET_COMPANY_LOADING} from "./company";

export const COMPANY_CERTIFICATE_DETAILS = 'user/company/certificate';
export const COMPANY_CERTIFICATE_LIST = 'user/company/certificate/list';
export const COMPANY_CERTIFICATE_UPDATE = 'user/company/certificate/update';
export const COMPANY_CERTIFICATE_LIST_UPDATE = 'user/company/certificate/list/update';
export const SET_COMPANY_CERTIFICATE = 'set/user/company/certificate';
export const COMPANY_CERTIFICATE_DELETE = 'set/user/company/certificate/list/delete/one';
export const SET_COMPANY_CERTIFICATE_LIST = 'set/user/company/certificate/list';
export const SET_COMPANY_CERTIFICATE_LIST_UPDATE = 'set/user/company/certificate/list/update';
export const COMPANY_CERTIFICATE_LIST_REMOVE_ONE = 'set/user/company/certificate/list/delete/one';
export const SET_COMPANY_CERTIFICATE_LOADING = 'set/user/company/certificate/loading';


export const state = () => ({
  certificates: [],
  certificate: {},
  loading: false
});

// getters
export const getters = {
  certificates: state => state.certificates,
  certificate: state => state.certificate,
  certificateLoading: state => state.loading
};

// mutations

export const mutations = {
  [SET_COMPANY_CERTIFICATE]: (state, payload = {}) => {
    state.certificate = payload
    state.certificate.form = true;
  },

  [SET_COMPANY_CERTIFICATE_LIST_UPDATE]: (state, payload) => {
    let index = state.certificates.findIndex(value => {
      return value.id === payload.id;
    });

    if (index >= 0) {
      vue.set(state.certificates, index, payload);
    } else
      state.certificates.push(payload);

    state.certificate = {form: false};
  },
  //photo list mutation
  [SET_COMPANY_CERTIFICATE_LIST]: (state, payload) => {
    state.certificates = payload ? payload : state.certificates
  },
  [SET_COMPANY_CERTIFICATE_LOADING]: (state, payload) => state.loading = payload,

  [COMPANY_CERTIFICATE_LIST_REMOVE_ONE]: (state, index) => {
    index = state.certificates.findIndex(value => {
      return value.id === index;
    });
    state.certificates.splice(index, 1);
  },
};

// actions
export const actions = {
  [COMPANY_CERTIFICATE_LIST](context) {
    if (!context.state.certificates.length) {
      this.app.$axios.get('comp_certificates?company_id=' + context.getters.user.company.id).then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE_LIST, data.result)
      });
    }
  },

  [COMPANY_CERTIFICATE_LIST_UPDATE](context, certificate) {
    this.app.$axios.post('comp_certificates', certificate).then(({data}) => {
      context.commit(SET_COMPANY_CERTIFICATE_LIST_UPDATE, data.result)
    });
  },
  [COMPANY_CERTIFICATE_DELETE](context, certificateId) {
    this.app.$axios.delete('comp_certificates/' + certificateId).then(({data}) => {
      context.commit(COMPANY_CERTIFICATE_LIST_REMOVE_ONE, certificateId)
    });
  },

//single
  [COMPANY_CERTIFICATE_DETAILS](context, certificateId = false) {
    if (certificateId) {
      return this.app.$axios.get('comp_certificates/' + certificateId).then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE, data.result)
      });
    }
    return context.commit(SET_COMPANY_CERTIFICATE);
  },
  [COMPANY_CERTIFICATE_UPDATE](context, certificate) {
    context.commit(SET_COMPANY_CERTIFICATE_LOADING, true);
    if (certificate.get('id'))
      certificate.append('_method', 'put')
    this.app.$axios.post('comp_certificates' + (certificate.get('id') ? '/' + certificate.get('id') : ''), certificate)
      .then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE_LOADING, false);
        context.commit(SET_COMPANY_CERTIFICATE_LIST_UPDATE, data.result)
      }).catch(err => {
      context.commit(SET_COMPANY_CERTIFICATE_LOADING, false);
    })
  }
};
export default {
  state,
  actions,
  mutations,
  getters
};
