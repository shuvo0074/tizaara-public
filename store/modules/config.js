export const SYSTEM_CONFIG_LIST = 'configs/system/default';
export const SET_SYSTEM_CONFIG_LIST = 'set/configs/system/default';
export const SET_MOBILE_NAV = 'set/configs/mobile/nav';
export const SET_SET_SYSTEM_CONFIG_API_CALLING = 'configs/system/default/api/calling';

export const state = () => ({
  configs: [],
  callApi: false,
  mobileNav: {
    'show-nav': false,
    'products-card-left-show-nav': false,
    'side-panel-show-nav': false,
    'personal-Info-left-bar': false
  }

});

export const getters = {
  configs: state => state.configs,
  mobileNav: state => state.mobileNav,

  configByAliasValue: state => (alias, index) => {
    let data = state.configs.find(v => v.alias === alias);
    return data && data.data && data.data[index] ? data.data[index].label : '';
  },
  configByAlias: state => alias => {
    let data = state.configs.find(v => v.alias === alias);
    return data ? data.data : [];
  }
};

export const actions = {
  [SYSTEM_CONFIG_LIST](context) {
    if (!context.state.configs.length && !context.state.callApi) {
      context.commit(SET_SET_SYSTEM_CONFIG_API_CALLING, true);

      this.app.$axios.get('/system_config').then(({data}) => {
        context.commit(SET_SYSTEM_CONFIG_LIST, data.result)
        context.commit(SET_SET_SYSTEM_CONFIG_API_CALLING, false);
      })
    }
  },
  [SET_MOBILE_NAV](context, payload) {
    context.commit(SET_MOBILE_NAV, payload)
  }
};

export const mutations = {
  [SET_SYSTEM_CONFIG_LIST]: (state, payload) => state.configs = payload,
  [SET_MOBILE_NAV]: (state, payload) => state.mobileNav[payload.index] = (payload.status === 'toggle' ? !state.mobileNav[payload.index] : payload.status),
  [SET_SET_SYSTEM_CONFIG_API_CALLING]: (state, payload) => state.callApi = payload
}

export default {
  state,
  actions,
  mutations,
  getters
};
