import vue from 'vue';

export const COMPANY_PORT_DETAILS = 'user/company/port';
export const COMPANY_PORT_LIST = 'user/company/port/list';
export const COMPANY_PORT_UPDATE = 'user/company/port/update';
export const COMPANY_PORT_LIST_UPDATE = 'user/company/port/list/update';
export const SET_COMPANY_CERTIFICATE = 'set/user/company/port';
export const COMPANY_PORT_DELETE = 'set/user/company/port/list/delete/one';
export const SET_COMPANY_PORT_LIST = 'set/user/company/port/list';
export const SET_COMPANY_PORT_LIST_UPDATE = 'set/user/company/port/list/update';
export const COMPANY_PORT_LIST_REMOVE_ONE = 'set/user/company/port/list/delete/one';

export const state = () => ({
  ports: [],
  port: {}
});

// getters
export const getters = {
  ports: state => state.ports,
  port: state => state.port
};

// mutations
export const mutations = {
  [SET_COMPANY_CERTIFICATE]: (state, payload = {}) => {
    state.port = payload
    state.port.form = true;
  },
  [SET_COMPANY_PORT_LIST_UPDATE]: (state, payload) => {
    let index = state.ports.findIndex(value => {
      return value.id === payload.id;
    });

    if (index >= 0) {
      vue.set(state.ports, index, payload);
    } else
      state.ports.push(payload);

    state.port = {form: false};
  },
  //photo list mutation
  [SET_COMPANY_PORT_LIST]: (state, payload) => {
    state.ports = payload ? payload : state.ports
  },

  [COMPANY_PORT_LIST_REMOVE_ONE]: (state, index) => {
    index = state.ports.findIndex(value => {
      return value.id === index;
    });
    state.ports.splice(index, 1);
  },
};

// actions
export const actions = {
  [COMPANY_PORT_LIST](context) {
    if (!context.state.ports.length) {
      this.app.$axios.get('comp_nearest_ports?company_id=' + context.getters.user.company.id).then(({data}) => {
        context.commit(SET_COMPANY_PORT_LIST, data.result)
      });
    }
  },

  [COMPANY_PORT_LIST_UPDATE](context, port) {
    this.app.$axios.post('comp_nearest_ports', port).then(({data}) => {
      context.commit(SET_COMPANY_PORT_LIST_UPDATE, data.result)
    });
  },
  [COMPANY_PORT_DELETE](context, portId) {
    this.app.$axios.delete('comp_nearest_ports/' + portId).then(({data}) => {
      context.commit(COMPANY_PORT_LIST_REMOVE_ONE, portId)
    });
  },

//single
  [COMPANY_PORT_DETAILS](context, portId = false) {
    if (portId) {
      return this.app.$axios.get('comp_nearest_ports/' + portId).then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE, data.result)
      });
    }
    return context.commit(SET_COMPANY_CERTIFICATE);
  },
  [COMPANY_PORT_UPDATE](context, port) {
    if (port.id)
      port._method = 'put';

    this.app.$axios.post('comp_nearest_ports' + (port.id ? '/'+port.id : ''), port).then(({data}) => {
      context.commit(SET_COMPANY_PORT_LIST_UPDATE, data.result)
    });
  }
};
export default {
  state,
  actions,
  mutations,
  getters
};
