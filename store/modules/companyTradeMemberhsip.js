import vue from 'vue'

export const COMPANY_TRADE_MEMBERSHIP_DETAILS = 'user/company/tradeMembership';
export const COMPANY_TRADE_MEMBERSHIP_LIST = 'user/company/tradeMembership/list';
export const COMPANY_TRADE_MEMBERSHIP_UPDATE = 'user/company/tradeMembership/update';
export const COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE = 'user/company/tradeMembership/list/update';
export const SET_COMPANY_CERTIFICATE = 'set/user/company/tradeMembership';
export const COMPANY_TRADE_MEMBERSHIP_DELETE = 'set/user/company/tradeMembership/list/delete/one';
export const SET_COMPANY_TRADE_MEMBERSHIP_LIST = 'set/user/company/tradeMembership/list';
export const SET_COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE = 'set/user/company/tradeMembership/list/update';
export const COMPANY_TRADE_MEMBERSHIP_LIST_REMOVE_ONE = 'set/user/company/tradeMembership/list/delete/one';

export const state = () => ({
  tradeMemberships: [],
  tradeMembership: {}
});

// getters
export const getters = {
  tradeMemberships: state => state.tradeMemberships,
  tradeMembership: state => state.tradeMembership
};

// mutations

export const mutations = {
  [SET_COMPANY_CERTIFICATE]: (state, payload = {}) => {
    state.tradeMembership = payload
    state.tradeMembership.form = true;
  },

  [SET_COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE]: (state, payload) => {
    let index = state.tradeMemberships.findIndex(value => {
      return value.id === payload.id;
    });

    if (index >= 0) {
      vue.set(state.tradeMemberships, index, payload);
    } else
      state.tradeMemberships.push(payload);

    state.tradeMembership = {form: false};
  },
  //photo list mutation
  [SET_COMPANY_TRADE_MEMBERSHIP_LIST]: (state, payload) => {
    state.tradeMemberships = payload ? payload : state.tradeMemberships
  },

  [COMPANY_TRADE_MEMBERSHIP_LIST_REMOVE_ONE]: (state, index) => {
    index = state.tradeMemberships.findIndex(value => {
      return value.id === index;
    });
    state.tradeMemberships.splice(index, 1);
  },
};

// actions
export const actions = {
  [COMPANY_TRADE_MEMBERSHIP_LIST](context) {
    if (!context.state.tradeMemberships.length) {
      this.app.$axios.get('comp_trade_memberships?company_id=' + context.getters.user.company.id).then(({data}) => {
        context.commit(SET_COMPANY_TRADE_MEMBERSHIP_LIST, data.result)
      });
    }
  },

  [COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE](context, tradeMembership) {
    this.app.$axios.post('comp_trade_memberships', tradeMembership).then(({data}) => {
      context.commit(SET_COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE, data.result)
    });
  },
  [COMPANY_TRADE_MEMBERSHIP_DELETE](context, tradeId) {
    this.app.$axios.delete('comp_trade_memberships/' + tradeId).then(({data}) => {
      context.commit(COMPANY_TRADE_MEMBERSHIP_LIST_REMOVE_ONE, tradeId)
    });
  },

//single
  [COMPANY_TRADE_MEMBERSHIP_DETAILS](context, tradeId = false) {
    if (tradeId) {
      return this.app.$axios.get('comp_trade_memberships/' + tradeId).then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE, data.result)
      });
    }
    return context.commit(SET_COMPANY_CERTIFICATE);
  },
  [COMPANY_TRADE_MEMBERSHIP_UPDATE](context, tradeMembership) {
    if (tradeMembership.id)
      tradeMembership._method = 'put';
    this.app.$axios.post('comp_trade_memberships' + (tradeMembership.id ? '/'+tradeMembership.id : ''), tradeMembership).then(({data}) => {
      context.commit(SET_COMPANY_TRADE_MEMBERSHIP_LIST_UPDATE, data.result)
    });
  }
};
export default {
  state,
  actions,
  mutations,
  getters
};
