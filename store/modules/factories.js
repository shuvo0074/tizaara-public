import vue from 'vue'
import {deepCopy} from "../../core/services/helper";
import address from "../../core/services/address";

export const COMPANY_FACTORY_DETAILS = 'user/company/factory';
export const COMPANY_FACTORY_LIST = 'user/company/factory/list';
export const COMPANY_FACTORY_UPDATE = 'user/company/factory/update';
export const COMPANY_FACTORY_LIST_UPDATE = 'user/company/factory/list/update';
export const SET_COMPANY_FACTORIES = 'set/user/company/factory';
export const COMPANY_FACTORY_DELETE = 'set/user/company/factory/list/delete/one';
export const SET_COMPANY_FACTORY_LIST = 'set/user/company/factory/list';
export const SET_COMPANY_FACTORY_LIST_UPDATE = 'set/user/company/factory/list/update';
export const COMPANY_FACTORY_LIST_REMOVE_ONE = 'set/user/company/factory/list/delete/one';
const initialFactory = {
  form: false
}

export const state = () => ({
  factories: [],
  factory: initialFactory,
});

// getters
export const getters = {
  factories: state => state.factories,
  factory: state => state.factory
};

// mutations

export const mutations = {
  [SET_COMPANY_FACTORIES]: (state, payload = {}) => {
    state.factory = payload
    state.factory.form = true;
  },

  [SET_COMPANY_FACTORY_LIST_UPDATE]: (state, payload) => {
    let index = state.factories.findIndex(value => {
      return value.id === payload.id;
    });
    if (index >= 0) {
      vue.set(state.factories, index, payload);
    } else {
      state.factories.push(payload);
    }

    state.factory = {form: false};
  },
  //photo list mutation
  [SET_COMPANY_FACTORY_LIST]: (state, payload) => {
    state.factories = payload ? payload : state.factories
  },

  [COMPANY_FACTORY_LIST_REMOVE_ONE]: (state, index) => {
    index = state.factories.findIndex(value => {
      return value.id === index;
    });
    state.factories.splice(index, 1);
  },
};

// actions
export const actions = {
  [COMPANY_FACTORY_LIST](context) {
    if (!context.state.factories.length) {
      this.app.$axios.get('comp_factories?company_id=' + context.getters.user.company.id).then(({data}) => {
        context.commit(SET_COMPANY_FACTORY_LIST, data.result)
      });
    }
  },

  [COMPANY_FACTORY_LIST_UPDATE](context, factory) {
    this.app.$axios.post('comp_factories', factory).then(({data}) => {
      context.commit(SET_COMPANY_FACTORY_LIST_UPDATE, data.result)
    });
  },
  [COMPANY_FACTORY_DELETE](context, factoryId) {
    this.app.$axios.delete('comp_factories/' + factoryId).then(({data}) => {
      context.commit(COMPANY_FACTORY_LIST_REMOVE_ONE, factoryId)
    });
  },

//single
  [COMPANY_FACTORY_DETAILS](context, factoryId = false) {
    if (factoryId) {
      return this.app.$axios.get('comp_factories/' + factoryId).then(({data}) => {
        context.commit(SET_COMPANY_FACTORIES, data.result)
      });
    }
    return context.commit(SET_COMPANY_FACTORIES, initialFactory);
  },
  [COMPANY_FACTORY_UPDATE](context, factory) {
    if (factory.id)
      factory._method = 'put';

    this.app.$axios.post('comp_factories' + (factory.id ? '/'+factory.id : ''), factory).then(({data}) => {
      context.commit(SET_COMPANY_FACTORY_LIST_UPDATE, data.result)
    });
  }
};
export default {
  state,
  actions,
  mutations,
  getters
};
