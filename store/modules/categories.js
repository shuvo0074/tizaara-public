export const CATEGORY_LIST = 'category/list';
export const SET_CATEGORY_LIST = 'set/category/list';
export const SET_CATEGORY_API_CALLING = 'category/list/api/calling';

export const state = () => ({
  categories: [],
  callApi: false,
});

export const getters = {
  categories: state => state.categories,
  allCategories: state => allCategories(state.categories),
  categoryChildrenById: state => category => {
    let data = state.categories.find(v => v.id === category);
    return data.children ? data.children : [];
  },
  immediateParentById: state => category => state.categories.find(v => v.parent_id === category),
  parentCategoriesById: state => category => findParent(allCategories(state.categories), category).reverse(),
  parentCategories: state => state.categories.filter(v => v.parent_id === null),
};

export const actions = {
  [CATEGORY_LIST](context) {
    if (!context.state.categories.length && !context.state.callApi) {
      context.commit(SET_CATEGORY_API_CALLING, true);
      this.app.$axios.get('/category').then(({data}) => {
        context.commit(SET_CATEGORY_LIST, data.result)
        context.commit(SET_CATEGORY_API_CALLING, false);
      })
    }
  },
};

export const mutations = {
  [SET_CATEGORY_LIST]: (state, payload) => state.categories = payload,
  [SET_CATEGORY_API_CALLING]: (state, payload) => state.callApi = payload
}

export default {
  state,
  actions,
  mutations,
  getters
};

function findParent(categories, category, data = []) {
  categories.map((v, i) => {
    if (v.id === category) {
      data.push(v)
      findParent(categories, v.parent_id, data)
    }
  })
  return data;
}

function allCategories(categories, data = []) {
  categories.map(e => {
    data.push(e);
    if (e.children) {
      allCategories(e.children, data)
    }
  });
  return data;
}
