import vue from 'vue'

export const COMPANY_TRADE_DETAILS = 'user/company/trade';
export const COMPANY_TRADE_LIST = 'user/company/trade/list';
export const COMPANY_TRADE_UPDATE = 'user/company/trade/update';
export const COMPANY_TRADE_LIST_UPDATE = 'user/company/trade/list/update';
export const SET_COMPANY_CERTIFICATE = 'set/user/company/trade';
export const COMPANY_TRADE_DELETE = 'set/user/company/trade/list/delete/one';
export const SET_COMPANY_TRADE_LIST = 'set/user/company/trade/list';
export const SET_COMPANY_TRADE_LIST_UPDATE = 'set/user/company/trade/list/update';
export const COMPANY_TRADE_LIST_REMOVE_ONE = 'set/user/company/trade/list/delete/one';

export const state = () => ({
  trades: [],
  trade: {}
});

// getters
export const getters = {
  trades: state => state.trades,
  trade: state => state.trade
};

// mutations

export const mutations = {
  [SET_COMPANY_CERTIFICATE]: (state, payload = {}) => {
    state.trade = payload
    state.trade.form = true;
  },

  [SET_COMPANY_TRADE_LIST_UPDATE]: (state, payload) => {
    let index = state.trades.findIndex(value => {
      return value.id === payload.id;
    });

    if (index >= 0) {
      vue.set(state.trades, index, payload);
    } else
      state.trades.push(payload);

    state.trade = {form: false};
  },
  //photo list mutation
  [SET_COMPANY_TRADE_LIST]: (state, payload) => {
    state.trades = payload ? payload : state.trades
  },

  [COMPANY_TRADE_LIST_REMOVE_ONE]: (state, index) => {
    index = state.trades.findIndex(value => {
      return value.id === index;
    });
    state.trades.splice(index, 1);
  },
};

// actions
export const actions = {
  [COMPANY_TRADE_LIST](context) {
    if (!context.state.trades.length) {
      this.app.$axios.get('comp_trade_infos?company_id=' + context.getters.user.company.id).then(({data}) => {
        context.commit(SET_COMPANY_TRADE_LIST, data.result)
      });
    }
  },

  [COMPANY_TRADE_LIST_UPDATE](context, trade) {
    this.app.$axios.post('comp_trade_infos', trade).then(({data}) => {
      context.commit(SET_COMPANY_TRADE_LIST_UPDATE, data.result)
    });
  },
  [COMPANY_TRADE_DELETE](context, tradeId) {
    this.app.$axios.delete('comp_trade_infos/' + tradeId).then(({data}) => {
      context.commit(COMPANY_TRADE_LIST_REMOVE_ONE, tradeId)
    });
  },

//single
  [COMPANY_TRADE_DETAILS](context, tradeId = false) {
    if (tradeId) {
      return this.app.$axios.get('comp_trade_infos/' + tradeId).then(({data}) => {
        context.commit(SET_COMPANY_CERTIFICATE, data.result)
      });
    }
    return context.commit(SET_COMPANY_CERTIFICATE);
  },
  [COMPANY_TRADE_UPDATE](context, trade) {
    if (trade.id)
      trade._method = 'put';
    this.app.$axios.post('comp_trade_infos' + (trade.id ? '/'+trade.id : ''), trade).then(({data}) => {
      context.commit(SET_COMPANY_TRADE_LIST_UPDATE, data.result)
    });
  }
};
export default {
  state,
  actions,
  mutations,
  getters
};
