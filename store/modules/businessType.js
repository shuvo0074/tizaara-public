export const BUSINESS_TYPE_LIST = 'businessTypes/business_type';
export const SET_BUSINESS_TYPE_LIST = 'set/businessTypes/business_type';
export const state = ()=>({
  businessTypes: []
});

export const getters = {
  businessTypes: state => state.businessTypes
};

export const actions = {
  [BUSINESS_TYPE_LIST](context) {
    if (!context.state.businessTypes.length) {
      this.app.$axios.get('/btype').then(({data}) => {
        context.commit(SET_BUSINESS_TYPE_LIST, data.result)
      })
    }
  }
};

export const mutations = {
  [SET_BUSINESS_TYPE_LIST]: (state, payload) => state.businessTypes = payload,
}

export default {
  state,
  actions,
  mutations,
  getters
};
