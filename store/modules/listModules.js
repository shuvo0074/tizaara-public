export const TURN_OVER_LIST = 'turnover_breakdowns';
export const SET_TURN_OVER_LIST = 'set/turnover_breakdowns';

export const RND_STAFF_LIST = 'rnd_staffs_breakdowns';
export const SET_RND_STAFF_LIST = 'set/rnd_staffs_breakdowns';

export const REVENUE_LIST = 'revenues_breakdowns';
export const SET_REVENUE_LIST = 'set/revenues_breakdowns';

export const EXPORT_PERCENTAGE_LIST = 'export_percentages_breakdowns';
export const SET_EXPORT_PERCENTAGE_LIST = 'set/export_percentages_breakdowns';

export const QC_STAFF_LIST = 'qc_staffs_breakdowns';
export const SET_QC_STAFF_LIST = 'set/qc_staffs_breakdowns';

export const state = () => ({
  turnover_breakdowns: [],
  rnd_staffs: [],
  revenues: [],
  export_percentages: [],
  qc_staffs: []
});

export const getters = {
  turnover_breakdowns: state => state.turnover_breakdowns,
  rnd_staffs: state => state.rnd_staffs,
  revenues: state => state.revenues,
  export_percentages: state => state.export_percentages,
  qc_staffs: state => state.qc_staffs
};

export const actions = {
  [TURN_OVER_LIST](context) {
    if (!context.state.turnover_breakdowns.length) {
      this.app.$axios.get('/turnover_breakdowns').then(({data}) => {
        context.commit(SET_TURN_OVER_LIST, data.result)
      })
    }
  },
  [RND_STAFF_LIST](context) {
    if (!context.state.rnd_staffs.length) {
      this.app.$axios.get('/rnd_staff_breakdowns').then(({data}) => {
        context.commit(SET_RND_STAFF_LIST, data.result)
      })
    }
  },
  [REVENUE_LIST](context) {
    if (!context.state.revenues.length) {
      this.app.$axios.get('/revenue_breakdowns').then(({data}) => {
        context.commit(SET_REVENUE_LIST, data.result)
      })
    }
  },
  [EXPORT_PERCENTAGE_LIST](context) {
    if (!context.state.export_percentages.length) {
      this.app.$axios.get('/export_percentage_breakdowns').then(({data}) => {
        context.commit(SET_EXPORT_PERCENTAGE_LIST, data.result)
      })
    }
  },
  [QC_STAFF_LIST](context) {
    if (!context.state.qc_staffs.length) {
      this.app.$axios.get('/qc_staff_breakdowns').then(({data}) => {
        context.commit(SET_QC_STAFF_LIST, data.result)
      })
    }
  }
};

export const mutations = {
  [SET_TURN_OVER_LIST]: (state, payload) => state.turnover_breakdowns = payload,
  [SET_RND_STAFF_LIST]: (state, payload) => state.rnd_staffs = payload,
  [SET_REVENUE_LIST]: (state, payload) => state.revenues = payload,
  [SET_EXPORT_PERCENTAGE_LIST]: (state, payload) => state.export_percentages = payload,
  [SET_QC_STAFF_LIST]: (state, payload) => state.qc_staffs = payload,
}

export default {
  state,
  actions,
  mutations,
  getters
};
