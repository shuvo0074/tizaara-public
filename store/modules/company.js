import {deepCopy} from "../../core/services/helper";
import address from "../../core/services/address";

export const COMPANY_BASIC = 'user/company';
export const COMPANY_BASIC_UPDATE = 'user/company/update';
export const SET_COMPANY_BASIC = 'set/user/company';

export const COMPANY_DETAILS = 'user/company/details';
export const COMPANY_DETAILS_UPDATE = 'user/company/details/update';
export const SET_COMPANY_DETAILS = 'set/user/company/details';

export const COMPANY_PHOTO_LIST = 'user/company/photo_list';
export const COMPANY_PHOTO_LIST_UPDATE = 'user/company/photo_list/update';
export const SET_COMPANY_PHOTO_LIST = 'set/user/company/photo_list';
export const SET_COMPANY_PHOTO_LIST_UPDATE = 'set/user/company/photo_list/update';
export const COMPANY_PHOTO_DELETE = 'set/user/company/photo_list/delete/single';
export const COMPANY_PHOTO_LIST_REMOVE_ONE = 'set/user/company/photo_list/update/delete';
export const SET_COMPANY_LOADING = 'set/user/company/loading';

export const state = () => ({
    company: {
        business_type_id: [],
        register_address: deepCopy(address),
        operational_address: deepCopy(address)
    },
    details: {},
    photos: [],
    loading: false
});

export const getters = {
    company: state => state.company,
    companyPhotos: state => state.photos,
    companyDetails: state => state.details,
    companyLoading: state => state.loading
};

export const actions = {
    [COMPANY_BASIC](context) {
        if (!context.state.company.name) {
            this.app.$axios.get('account/company').then(({data}) => {
                context.commit(SET_COMPANY_BASIC, data.result)
            });
        }
    },
    [COMPANY_BASIC_UPDATE](context, company) {
        context.commit(SET_COMPANY_LOADING, true);
        if (company.id)
            company._method = 'put';

        this.app.$axios.post('company' + (context.getters.user.company && context.getters.user.company.id ? '/' + context.getters.user.company.id : ''), company)
            .then(({data}) => {
                context.commit(SET_COMPANY_BASIC, data.result);
                let user = deepCopy(this.$auth.user);
                user.company = data.result;
                this.$auth.setUser(user);
                context.commit(SET_COMPANY_LOADING, false);
            }).catch(err => {
            context.commit(SET_COMPANY_LOADING, false);
        })
    },

    //company details action
    [COMPANY_DETAILS](context) {
        if (!context.state.details.about_us) {
            this.app.$axios.get('company/company_details/' + context.getters.user.company.id).then(({data}) => {
                context.commit(SET_COMPANY_DETAILS, data.result)
            });
        }
    },
    [COMPANY_DETAILS_UPDATE](context, company) {
        context.commit(SET_COMPANY_LOADING, true);
        this.app.$axios.post('company/company_details', company).then(({data}) => {
            context.commit(SET_COMPANY_DETAILS, data.result);
            context.commit(SET_COMPANY_LOADING, false);
        }).catch(err=>{
            context.commit(SET_COMPANY_LOADING, false);
        })
    },

    //company photo list
    [COMPANY_PHOTO_LIST](context) {
        if (!context.state.photos.length) {
            this.app.$axios.get('comp_photos?company_id=' + context.getters.user.company.id).then(({data}) => {
                context.commit(SET_COMPANY_PHOTO_LIST, data.result)
            });
        }
    },

    [COMPANY_PHOTO_LIST_UPDATE](context, photo) {
        this.app.$axios.post('comp_photos', photo).then(({data}) => {
            context.commit(SET_COMPANY_PHOTO_LIST_UPDATE, data.result)
        });
    },
    [COMPANY_PHOTO_DELETE](context, photoId) {
        this.app.$axios.delete('comp_photos/' + photoId).then(({data}) => {
            context.commit(COMPANY_PHOTO_LIST_REMOVE_ONE, photoId)
        });
    }
};

export const mutations = {
    [SET_COMPANY_BASIC]: (state, payload) => {
        state.company = payload ? payload : state.company
        state.company.business_type_id = state.company.business_type_id ? state.company.business_type_id : [];
        state.company.register_address = state.company.register_address ? state.company.register_address : deepCopy(address);
        state.company.operational_address = state.company.operational_address ? state.company.operational_address : deepCopy(address);
    },

    [SET_COMPANY_DETAILS]: (state, payload) => state.details = payload ? payload : state.details,

    //photo list mutation
    [SET_COMPANY_PHOTO_LIST]: (state, payload) => state.photos = payload ? payload : state.photos,
    [SET_COMPANY_PHOTO_LIST_UPDATE]: (state, payload) => state.photos.push(payload),
    [SET_COMPANY_LOADING]: (state, payload) => state.loading = payload,

    [COMPANY_PHOTO_LIST_REMOVE_ONE]: (state, index) => {
        index = state.photos.findIndex(value => value.id === index);
        state.photos.splice(index, 1);
    },
}

export default {
    state,
    actions,
    mutations,
    getters
};
