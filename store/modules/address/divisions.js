export const DIVISION_LIST = 'division/list';
export const SET_DIVISION_LIST = 'set/division/list';
export const SET_DIVISION_API_CALLING = 'set/division/list/api/calling';
export const state = () => ({
  divisions: [],
  callApi: false
});

export const getters = {
  divisions: state => state.divisions,
  divisionsByCountry: state => country => state.divisions.filter(v => v.country_id === country)
};

export const actions = {
  [DIVISION_LIST](context, country_id) {
    if (!!country_id && !context.state.divisions.find(v => v.country_id === country_id) && !context.state.callApi) {
      context.commit(SET_DIVISION_API_CALLING, true)
      this.app.$axios.get(`division?country_id=${country_id}`).then(({data}) => {
        context.commit(SET_DIVISION_LIST, data.result)
        context.commit(SET_DIVISION_API_CALLING, false)
      })
    }
  }
};

export const mutations = {
  [SET_DIVISION_LIST]: (state, payload) => state.divisions ? state.divisions = [...state.divisions, ...payload] : '',
  [SET_DIVISION_API_CALLING]: (state, payload) => state.callApi = payload,
}

export default {
  state,
  actions,
  mutations,
  getters
};
