export const AREA_LIST = 'area/list';
export const SET_AREA_LIST = 'set/area/list';
export const SET_AREA_API_CALLING = 'set/area/list/api/calling';

export const state = () => ({
  areas: [],
  callApi: false
});

export const getters = {
  areas: state => state.areas,
  areasByCity: state => city => state.areas.filter(v => v.city_id === city)
};

export const actions = {
  [AREA_LIST](context, city_id) {
    if (!!city_id && !context.state.callApi && !context.state.areas.find(v => v.city_id === city_id)) {
      context.commit(SET_AREA_API_CALLING, true)
      this.app.$axios.get(`area?city_id=${city_id}`).then(({data}) => {
        context.commit(SET_AREA_LIST, data.result)
        context.commit(SET_AREA_API_CALLING, false)
      })
    }
  }
};

export const mutations = {
  [SET_AREA_LIST]: (state, areas) => areas ? state.areas = [...state.areas, ...areas] : '',
  [SET_AREA_API_CALLING]: (state, payload) => state.callApi = payload,
}

export default {
  state,
  actions,
  mutations,
  getters
};
