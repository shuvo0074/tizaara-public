export const COUNTRY_LIST = 'country/list';
export const SET_COUNTRY_LIST = 'set/country/list';
export const SET_COUNTRY_API_CALLING = 'set/country/list/api/calling';
export const state = () => ({
  countries: [],
  callApi: false
});

export const getters = {
  countries: state => state.countries
};

export const actions = {
  [COUNTRY_LIST](context) {
    if (!context.state.countries.length && !context.state.callApi) {
      context.commit(SET_COUNTRY_API_CALLING, true);
      this.app.$axios.get('country').then(({data}) => {
        context.commit(SET_COUNTRY_LIST, data.result);
        context.commit(SET_COUNTRY_API_CALLING, false);
      })
    }
  }
};

export const mutations = {
  [SET_COUNTRY_LIST]: (state, payload) => state.countries = payload,
  [SET_COUNTRY_API_CALLING]: (state, payload) => state.callApi = payload,
}

export default {
  state,
  actions,
  mutations,
  getters
};
