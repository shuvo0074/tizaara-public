import {SET_DIVISION_API_CALLING} from "./divisions";

export const CITY_LIST = 'city/list';
export const SET_CITY_LIST = 'set/city/list';
export const SET_CITY_API_CALLING = 'set/city/list/api/calling';
export const state = () => ({
    cities: [],
    callApi: false,
});

export const getters = {
    cities: state => state.cities,
    citiesByDivision: state => division => state.cities.filter(v => v.division_id === division)

};

export const actions = {
    [CITY_LIST](context, division_id) {
        if (!!division_id && !context.state.callApi && !context.state.cities.find(v => v.division_id === division_id)) {
            context.commit(SET_CITY_API_CALLING, true)
            this.app.$axios.get(`city?division_id=${division_id}`).then(({data}) => {
                context.commit(SET_CITY_LIST, data.result)
                context.commit(SET_CITY_API_CALLING, false)
            })
        }
    }
};

export const mutations = {
    [SET_CITY_LIST]: (state, cities) => cities ? state.cities = [...state.cities, ...cities] : '',
    [SET_CITY_API_CALLING]: (state, payload) => state.callApi = payload,

}

export default {
    state,
    actions,
    mutations,
    getters
};
