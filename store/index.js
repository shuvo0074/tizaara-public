import systemConfigs from "./modules/config";
import categories from "./modules/categories";
import countries from "./modules/address/countries";
import divisions from "./modules/address/divisions";
import cities from "./modules/address/cities";
import areas from "./modules/address/areas";
import company from "./modules/company";
import businessType from "./modules/businessType";
import moduleLists from "./modules/listModules";
import certifications from "./modules/certifications";
import factories from "./modules/factories";
import companyTrades from "./modules/companyTrade";
import companyTradeMembership from "./modules/companyTradeMemberhsip";
import companyPort from "./modules/companyPort";

export const state = () => ({})

export const getters = {
  authenticated(state) {
    return state.auth.loggedIn
  },
  user(state) {
    return state.auth.user
  },
  displayName({auth}) {
    let firstName = (auth.user.first_name || ''),
      lastName = (auth.user.last_name || ''),
      user = firstName + (firstName && lastName ? ' ' : '') + lastName;
    return user ? user : auth.user.userName;
  }
}

export const mutations = {}

export const action = {}

export const modules = {
  systemConfigs,
  categories,
  countries,
  divisions,
  cities,
  areas,
  company,
  businessType,
  moduleLists,
  certifications,
  companyTrades,
  companyTradeMembership,
  factories,
  companyPort
}
