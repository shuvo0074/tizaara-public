import Vue from 'vue'
import {baseApiUrl} from "../../core/config";

Vue.mixin({
  data() {
    return {
      baseAsset: baseApiUrl,
      config: {}
    }
  }
})
