import Vue from "vue";

export default function ({$axios, store, redirect}) {
  $axios.onError(error => {
    if (error.response.status === 422) {
      store.dispatch("validation/setErrors", error.response.data.errors);
    }

    if (error.response.data.message) {
      store.$toast.error(error.response.data.message);
    }

    if (error.response.status === 404) {
      store.dispatch("validation/setErrors", error.response.data.errors);
      //return redirect("/");
    }
    return Promise.reject(error);
  });

  $axios.onResponse(response => {

    /*if (response.status === 204) {
      store.$toast.info('No data exists');
    }*/

    if (response.data.message) {
      store.$toast.success(response.data.message);
    }

    return response;
  });

  $axios.onRequest(() => {
    store.dispatch("validation/clearErrors");
  });
}
